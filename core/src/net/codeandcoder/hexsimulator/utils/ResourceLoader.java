package net.codeandcoder.hexsimulator.utils;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.utils.I18NBundle;

public abstract class ResourceLoader {
    
    protected String texturePath;
    protected String soundPath;
    protected String musicPath;
    protected String i18nPath;
    
    public ResourceLoader(String texturesPath, String soundPath, String musicPath, String i18nPath) {
        this.texturePath = texturesPath;
        this.soundPath = soundPath;
        this.musicPath = musicPath;
        this.i18nPath = i18nPath;
    }
    
    public abstract void initResources();
    
    public abstract Sound getSound(String name);
    public abstract Music getMusic(String name);
    public abstract Texture getTexture(String name);
    public abstract I18NBundle getI18NBundle(String path);
    
    public abstract Texture getNoLoadedTexture(String path);
    public abstract void dispose();

}
