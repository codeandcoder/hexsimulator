package net.codeandcoder.hexsimulator.utils;

import java.util.Random;

public class RandomProvider {

    private long seed;
    private Random random;
    
    public RandomProvider(long seed) {
        this.seed = seed;
        random = new Random(seed);
    }
    
    public boolean flipCoin() {
        return random.nextInt() % 2 == 0;
    }
    
    public int randomNum(int from, int to) {
        return Math.abs(random.nextInt()) % (to - from) + from;
    }
    
    public long getSeed() {
        return seed;
    }
    
}
