package net.codeandcoder.hexsimulator.grid;

public interface GridNode {
    
    public abstract Coordinate getPosition();
    public abstract boolean isObstacle();
    
}