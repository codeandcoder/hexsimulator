package net.codeandcoder.hexsimulator.grid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import net.codeandcoder.hexsimulator.entities.Entity;

public class EntityGridNode implements GridNode {
    
    private Coordinate position;
    private List<Entity> entities = new ArrayList<Entity>();
    
    public EntityGridNode(int x, int y) {
        this.position = new Coordinate(x, y);
    }
    
    @Override
    public Coordinate getPosition() {
        return position;
    }

    @Override
    public boolean isObstacle() {
        boolean obstacle = false;
        for (int i = 0; i < entities.size() && !obstacle; i++) {
            obstacle = entities.get(i).isObstacle();
        }
        return obstacle;
    }
    
    public void addEntity(Entity e) {
        entities.add(e);
        Collections.sort(entities, new DrawPriorityComparator());
    }
    
    public boolean hasEntityOfType(Class<?> c) {
        for (Entity e : entities) {
            if (e.getClass().equals(c)) {
                return true;
            }
        }
        
        return false;
    }
    
    public void removeEntity(Entity e) {
        entities.remove(e);
    }
    
    public List<Entity> getEntities() {
        return entities;
    }
    
    public void drawPriorityChanged() {
        Collections.sort(entities, new DrawPriorityComparator());
    }
    
    public static class DrawPriorityComparator implements Comparator<Entity> {

        @Override
        public int compare(Entity o1, Entity o2) {
            if (o1.getDrawPriority() > o2.getDrawPriority()) {
                return 1;
            }
            
            return -1;
        }
        
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((position == null) ? 0 : position.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        EntityGridNode other = (EntityGridNode) obj;
        if (position == null) {
            if (other.position != null)
                return false;
        } else if (!position.equals(other.position))
            return false;
        return true;
    }

}
