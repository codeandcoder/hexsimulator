package net.codeandcoder.hexsimulator.grid;


public class Coordinate {
    
    private int x;
    private int y;
    
    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public void sumX(int num) {
        x += num;
    }

    public void sumY(int num) {
        y += num;
    }
    
    public double hexDistanceTo(Coordinate other) {
        return (Math.abs(x - other.getX()) 
                + Math.abs(x + y - other.getX() - other.getY())
                + Math.abs(y - other.getY())) / 2.0;
    }
    
    public Coordinate surroundingfromNumber(int num) {
        int resultX = x;
        int resultY = y;
        
        switch(num) {
        case 0:
            resultY += 2;
            break;
        case 1:
            resultY -= 2;
            break;
        case 2:
            resultY += 1;
            break;
        case 3:
            resultY -= 1;
            break;
        case 4:
            if (y % 2 == 0) {
                resultX += 1;
                resultY -= 1; 
            } else {
                resultX -= 1;
                resultY -= 1; 
            }
            break;
        case 5:
            if (y % 2 == 0) {
                resultX += 1;
                resultY += 1;
            } else {
                resultX -= 1;
                resultY += 1; 
            }
            break;
        }
        
        return new Coordinate(resultX, resultY);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + x;
        result = prime * result + y;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Coordinate other = (Coordinate) obj;
        if (x != other.x)
            return false;
        if (y != other.y)
            return false;
        return true;
    }
    
}
