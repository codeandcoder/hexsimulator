package net.codeandcoder.hexsimulator.grid;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import net.codeandcoder.hexsimulator.Controller;

public class HexGrid {

    private HashMap<String, GridNode> grid = new HashMap<String, GridNode>();
    private int sizeX;
    private int sizeY;
    
    public HexGrid (int sizeX, int sizeY, Controller controller) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        
        // Empty nodes building
        for (int y = 0; y < sizeY; y++) {
            for (int x = 0; x < sizeX; x++) {
                GridNode node = new EntityGridNode(x, y);
                grid.put(genKey(x,y), node);
            }
        }
    }
    
    public boolean isNodeInaccessible(Coordinate c) {
        boolean isInaccessible = true;
        
        List<Coordinate> coordsToCheck = new ArrayList<Coordinate>();
        coordsToCheck.add(new Coordinate(c.getX(), c.getY()-2));
        coordsToCheck.add(new Coordinate(c.getX(), c.getY()+2));
        coordsToCheck.add(new Coordinate(c.getX(), c.getY()-1));
        coordsToCheck.add(new Coordinate(c.getX(), c.getY()+1));
        if (c.getY() % 2 == 0) {
            coordsToCheck.add(new Coordinate(c.getX()+1, c.getY()-1));
            coordsToCheck.add(new Coordinate(c.getX()+1, c.getY()+1));
        } else {
            coordsToCheck.add(new Coordinate(c.getX()-1, c.getY()-1));
            coordsToCheck.add(new Coordinate(c.getX()-1, c.getY()+1));
        }
        
        for (Coordinate current : coordsToCheck) {
            isInaccessible = isInaccessible && (!hasNode(current) || getNode(current).isObstacle());
        }
        
        return isInaccessible;
    }
    
    public GridNode getNode(int x, int y) {
        return grid.get(genKey(x,y));
    }
    
    public GridNode getNode(Coordinate c) {
        return grid.get(genKey(c.getX(),c.getY()));
    }
    
    public boolean hasNode(Coordinate c) {
        return grid.containsKey(genKey(c.getX(), c.getY()));
    }
    
    private String genKey(int x, int y) {
        return x + ":" + y;
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }
    
}
