package net.codeandcoder.hexsimulator.pathfinding;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.codeandcoder.hexsimulator.entities.Entity;
import net.codeandcoder.hexsimulator.entities.interfaces.Feeder;
import net.codeandcoder.hexsimulator.grid.Coordinate;
import net.codeandcoder.hexsimulator.grid.EntityGridNode;
import net.codeandcoder.hexsimulator.grid.GridNode;
import net.codeandcoder.hexsimulator.grid.HexGrid;

import com.badlogic.gdx.math.Vector2;

public class AStarHexPathfinder extends HexPathfinder {

    public static final int MOVE_COST = 10;
    
    public AStarHexPathfinder(HexGrid hexGrid) {
        super(hexGrid);
    }

    @Override
    public List<Coordinate> findPath(GridNode origin, Coordinate target) {
        List<Coordinate> bestPath = new ArrayList<Coordinate>();
        
        List<AStarNode> visitedNodes = new ArrayList<AStarNode>();
        List<AStarNode> openNodes = new ArrayList<AStarNode>();
        AStarNode originNode = new AStarNode(origin, target);
        openNodes.add(originNode);
        
        boolean finished = false;
        
        while(!finished && !openNodes.isEmpty()) {
            AStarNode currentNode = openNodes.remove(0);
            if (currentNode.getGridNode().getPosition().equals(target)) {
                if (hexGrid.getNode(target).isObstacle() && currentNode.parent != null) {
                    currentNode = currentNode.parent;
                }
                while(currentNode.parent != null) {
                    bestPath.add(0, currentNode.getGridNode().getPosition());
                    currentNode = currentNode.parent;
                }
                finished = true;
            } else {
                checkNodes(currentNode, openNodes, visitedNodes, target);
                
                if (!openNodes.isEmpty()) {
                    visitedNodes.add(currentNode);
                    Collections.sort(openNodes);
                }
            }
        }
        
        return bestPath;
    }
    
    private void checkNodes(AStarNode currentNode, List<AStarNode> openNodes,
            List<AStarNode> visitedNodes, Coordinate target) {
        
        Coordinate cPos = currentNode.getGridNode().getPosition();
        List<Coordinate> positionsToCheck = new ArrayList<Coordinate>();
        positionsToCheck.add(new Coordinate(cPos.getX(), cPos.getY()-2));
        positionsToCheck.add(new Coordinate(cPos.getX(), cPos.getY()+2));
        positionsToCheck.add(new Coordinate(cPos.getX(), cPos.getY()+1));
        positionsToCheck.add(new Coordinate(cPos.getX(), cPos.getY()-1));
        if (cPos.getY() % 2 == 0) {
            positionsToCheck.add(new Coordinate(cPos.getX()+1, cPos.getY()-1));
            positionsToCheck.add(new Coordinate(cPos.getX()+1, cPos.getY()+1));
        } else {
            positionsToCheck.add(new Coordinate(cPos.getX()-1, cPos.getY()-1));
            positionsToCheck.add(new Coordinate(cPos.getX()-1, cPos.getY()+1));
        }
        
        for (int i = 0; i < positionsToCheck.size(); i++) {
            Coordinate pos = positionsToCheck.get(i);
            if (hexGrid.hasNode(pos)) {
                GridNode newGridNode = hexGrid.getNode(pos);
                AStarNode newNode = new AStarNode(newGridNode, target);
                newNode.parent = currentNode;
                if (!visitedNodes.contains(newNode)) {
                    int indexOfOpen = openNodes.indexOf(newNode);
                    if (indexOfOpen != -1) {
                        newNode = openNodes.get(indexOfOpen);
                        if (currentNode.calculateG(newNode) < newNode.parent.calculateG(newNode)) {
                            newNode.parent = currentNode;
                        }
                    } else {
                        if (!newGridNode.isObstacle() || newGridNode.getPosition().equals(target)) {
                            openNodes.add(newNode);
                        }
                    }
                }
            }
        }
    }

    @Override
    public Feeder findNearestFeeder(Coordinate origin) {
        float bestDistance = 100000;
        Feeder closest = null;
        List<Feeder> candidates = findFoodCandidates();
        for (Feeder f : candidates) {
            if (!f.isExhausted()) {
                Vector2 p1 = new Vector2(origin.getX(), origin.getY());
                Vector2 p2 = new Vector2(((Entity)f).getPosition().getX(), ((Entity)f).getPosition().getY());
                float currentDistance = Vector2.dst(p1.x, p1.y, p2.x, p2.y);
                if (closest == null || currentDistance < bestDistance) {
                    closest = f;
                    bestDistance = currentDistance;
                }
            }
        }
        return closest;
    }
    
    public List<Feeder> findFoodCandidates() {
        List<Feeder> feeders = new ArrayList<Feeder>();
        for (int y = 0; y < hexGrid.getSizeY(); y++) {
            for (int x = 0; x < hexGrid.getSizeX(); x++) {
                GridNode node = hexGrid.getNode(x,y);
                
                if ((node instanceof EntityGridNode) && !hexGrid.isNodeInaccessible(node.getPosition())) {
                    EntityGridNode entityNode = (EntityGridNode) node;
                    for (int i = 0; i < entityNode.getEntities().size(); i++) {
                        Entity e = entityNode.getEntities().get(i);
                        if (e instanceof Feeder) {
                            feeders.add((Feeder) e);
                        }
                    }
                }
            }
        }
        return feeders;
    }

}
