package net.codeandcoder.hexsimulator.pathfinding;
import net.codeandcoder.hexsimulator.grid.Coordinate;
import net.codeandcoder.hexsimulator.grid.GridNode;

public class AStarNode implements Comparable<AStarNode> {
    
    public AStarNode parent;
    private GridNode gridNode;
    private Coordinate target;
    
    public AStarNode (GridNode gridNode, Coordinate target) {
        this.gridNode = gridNode;
        this.target = target;
    }
    
    public int calculateG(AStarNode other) {
        if (other == null || parent == null) {
            return 0;
        } else if (other.equals(this)) {
            return parent.calculateG(parent.parent);
        }
        
        return parent.calculateG(parent.parent) + AStarHexPathfinder.MOVE_COST;
    }
    
    public int calculateH() {
        return (int) (gridNode.getPosition().hexDistanceTo(target) * AStarHexPathfinder.MOVE_COST);
    }
    
    @Override
    public int compareTo(AStarNode o) {
        int myScore = calculateG(this) + calculateH();
        int otherScore = o.calculateG(o) + o.calculateH();
        return myScore - otherScore;
    }
    
    public GridNode getGridNode() {
        return gridNode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((gridNode == null) ? 0 : gridNode.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        AStarNode other = (AStarNode) obj;
        if (gridNode == null) {
            if (other.gridNode != null)
                return false;
        } else if (!gridNode.equals(other.gridNode))
            return false;
        return true;
    }

}
