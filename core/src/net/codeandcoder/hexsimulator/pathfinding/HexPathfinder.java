package net.codeandcoder.hexsimulator.pathfinding;
import java.util.List;

import net.codeandcoder.hexsimulator.entities.interfaces.Feeder;
import net.codeandcoder.hexsimulator.grid.Coordinate;
import net.codeandcoder.hexsimulator.grid.GridNode;
import net.codeandcoder.hexsimulator.grid.HexGrid;

public abstract class HexPathfinder {

    protected HexGrid hexGrid;
    
    public HexPathfinder(HexGrid hexGrid) {
        this.hexGrid = hexGrid;
    }
    
    public abstract List<Coordinate> findPath(GridNode origin, Coordinate target);
    public abstract Feeder findNearestFeeder(Coordinate origin);
    
}
