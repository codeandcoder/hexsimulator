package net.codeandcoder.hexsimulator;

import java.util.ArrayList;
import java.util.List;

import net.codeandcoder.hexsimulator.UI.GUIAdapter;
import net.codeandcoder.hexsimulator.UI.GUIScreen;
import net.codeandcoder.hexsimulator.UI.GameScreen;
import net.codeandcoder.hexsimulator.UI.UIAdapter;
import net.codeandcoder.hexsimulator.entities.Person;
import net.codeandcoder.hexsimulator.grid.HexGrid;
import net.codeandcoder.hexsimulator.pathfinding.AStarHexPathfinder;
import net.codeandcoder.hexsimulator.pathfinding.HexPathfinder;
import net.codeandcoder.hexsimulator.utils.InternalFilesLoader;
import net.codeandcoder.hexsimulator.utils.RandomProvider;
import net.codeandcoder.hexsimulator.utils.ResourceLoader;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;

public class HexSimulator extends Game {

    // Resource paths
    public static final String TEXTURES_PATH = "textures";
    public static final String SOUND_PATH = "sound";
    public static final String MUSIC_PATH = "music";
    public static final String I18N_PATH = "I18N";
    
    public static final float INITIAL_SQUARE_SIZE = 12f;
    public static float CAMERA_SPEED = 500;
    public static float ITERATION_LENGTH = 0.01f;
    
    private long lastIterationTime = 0;
    private int currentIteration = 0;
    
    private Controller controller;
    private Person player;
    
    private List<GUIScreen> screens = new ArrayList<GUIScreen>();
    
    @Override
    protected void init() {
        HexGrid grid = new HexGrid(30, 60, controller);
        UIAdapter ui = new GUIAdapter(INITIAL_SQUARE_SIZE, camera, batch);
        HexPathfinder pathfinder = new AStarHexPathfinder(grid);
        RandomProvider rand = new RandomProvider(9223372036854775807L);
        
        controller = new Controller(grid, ui, pathfinder, resources, rand);
        player = WorldGenerator.generateWorld(controller, grid, rand);
        
        screens.add(GUIScreen.buildScreen(this, GameScreen.class));
    }
    
    public void restart(String seed, int sizeX, int sizeY) {
        HexGrid grid = new HexGrid(sizeX, sizeY, controller);
        UIAdapter ui = new GUIAdapter(INITIAL_SQUARE_SIZE, camera, batch);
        HexPathfinder pathfinder = new AStarHexPathfinder(grid);
        RandomProvider rand = new RandomProvider(Long.valueOf(seed));
        
        controller = new Controller(grid, ui, pathfinder, resources, rand);
        player = WorldGenerator.generateWorld(controller, grid, rand);
    }

    @Override
    protected void update(float deltaTime) {
        if (System.currentTimeMillis() - lastIterationTime >= ITERATION_LENGTH * 1000) {
            currentIteration++;
            lastIterationTime = System.currentTimeMillis();
            controller.update(currentIteration);
        }
        
        handleDebugInput(deltaTime);
        
        controller.render();
    }

    @Override
    protected void renderGUI() {
        for (int i = 0; i < screens.size(); i++) {
            screens.get(i).render(batch);
        }
    }

    @Override
    protected void destroy() {
        
    }

    @Override
    protected String getSplashScreenPath() {
        return "textures/splash/splash1.png";
    }

    @Override
    protected ResourceLoader getResourceLoader() {
        return new InternalFilesLoader(TEXTURES_PATH, SOUND_PATH, MUSIC_PATH, I18N_PATH);
    }
    
    private void handleDebugInput(float deltaTime) {
        if (Gdx.app.getType() != ApplicationType.Desktop)
            return;

        // Camera Controls (move)
        float camMoveSpeed = CAMERA_SPEED * deltaTime;
        if (Gdx.input.isKeyPressed(Keys.LEFT))
            camera.translate(-camMoveSpeed, 0);
        if (Gdx.input.isKeyPressed(Keys.RIGHT))
            camera.translate(camMoveSpeed, 0);
        if (Gdx.input.isKeyPressed(Keys.UP))
            camera.translate(0, camMoveSpeed);
        if (Gdx.input.isKeyPressed(Keys.DOWN))
            camera.translate(0, -camMoveSpeed);
        
//        if(Gdx.input.isButtonPressed(Input.Buttons.LEFT) && Gdx.input.justTouched()){
//            UIAdapter ui = controller.getUI();
//            if (ui instanceof GUIAdapter) {
//                EntityGridNode node = ((GUIAdapter) ui).getNodeInPixels(controller.getGrid(), Gdx.input.getX(), Gdx.input.getY());
//                if (node != null && player.getBrain().getTaskCount() < 2) {
//                    player.getBrain().addTask(new MoveTask("Move", 10, controller, player, node.getPosition()));
//                }
//            }
//        }
            
    }

    public Controller getController() {
        return controller;
    }

}
