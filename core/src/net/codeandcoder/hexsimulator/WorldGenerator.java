package net.codeandcoder.hexsimulator;

import net.codeandcoder.hexsimulator.entities.Entity;
import net.codeandcoder.hexsimulator.entities.FruitTreeEntity;
import net.codeandcoder.hexsimulator.entities.GrassEntity;
import net.codeandcoder.hexsimulator.entities.Person;
import net.codeandcoder.hexsimulator.entities.TreeEntity;
import net.codeandcoder.hexsimulator.entities.WaterEntity;
import net.codeandcoder.hexsimulator.grid.Coordinate;
import net.codeandcoder.hexsimulator.grid.EntityGridNode;
import net.codeandcoder.hexsimulator.grid.GridNode;
import net.codeandcoder.hexsimulator.grid.HexGrid;
import net.codeandcoder.hexsimulator.utils.RandomProvider;

public class WorldGenerator {

    public static Person generateWorld(Controller controller, HexGrid grid, RandomProvider randomProvider) {
        // Basic grass background surrounded by water
        for (int y = 0; y < grid.getSizeY(); y++) {
            for (int x = 0; x < grid.getSizeX(); x++) {
                GridNode node = grid.getNode(x,y);
                
                if (node instanceof EntityGridNode) {
                    EntityGridNode entityNode = (EntityGridNode) node;
                    if (y == 0 || y == 1 || y == grid.getSizeY() - 1 || y == grid.getSizeY() - 2
                            || x == 0 || x == grid.getSizeX() - 1) {
                        WaterEntity water = new WaterEntity(controller, "Water", new Coordinate(x,y));
                        water.init();
                        entityNode.addEntity(water);
                    } else {
                        Entity grass = new GrassEntity(controller, "Grass", new Coordinate(x,y));
                        grass.init();
                        entityNode.addEntity(grass);
                    }
                }
            }
        }
        
        // Person generator
        Coordinate person1Position = new Coordinate(2,2);
        Person person1 = new Person(controller, "Person1", person1Position);
        person1.init();
        EntityGridNode node1 = (EntityGridNode) grid.getNode(person1Position);
        node1.addEntity(person1);
        person1.setDrawPriority(2);
        
//        Coordinate person2Position = new Coordinate(18,20);
//        Person person2 = new Person(controller, "Person2", person2Position);
//        person2.init();
//        EntityGridNode node2 = (EntityGridNode) grid.getNode(person2Position);
//        node2.addEntity(person2);
//        person2.setDrawPriority(2);
        
//        Coordinate person3Position = new Coordinate(26,6);
//        Person person3 = new Person(controller, "Person3", person3Position);
//        person3.init();
//        EntityGridNode node3 = (EntityGridNode) grid.getNode(person3Position);
//        node3.addEntity(person3);
//        person3.setDrawPriority(2);
        
        // Water
        int minLakesNumber = (int) Math.round(grid.getSizeX() * grid.getSizeY() * 0.01);
        int maxLakesNumber = (int) Math.round(grid.getSizeX() * grid.getSizeY() * 0.012);
        int lakesNumber = randomProvider.randomNum(minLakesNumber, maxLakesNumber);
        for (int i = 0; i < lakesNumber; i++) {
            int x = randomProvider.randomNum(0, grid.getSizeX());
            int y = randomProvider.randomNum(0, grid.getSizeY());
            generateLake(controller, grid, new Coordinate(x, y), randomProvider);
        }
        
        // Forests
        int minForestNumber = (int) Math.round(grid.getSizeX() * grid.getSizeY() * 0.008);
        int maxForestNumber = (int) Math.round(grid.getSizeX() * grid.getSizeY() * 0.01);
        int forestNumber = randomProvider.randomNum(minForestNumber, maxForestNumber);
        for (int i = 0; i < forestNumber; i++) {
            int x = randomProvider.randomNum(0, grid.getSizeX());
            int y = randomProvider.randomNum(0, grid.getSizeY());
            generateForest(controller, grid, new Coordinate(x, y), randomProvider);
        }
        
        return person1;
    };
    
    
    private static void generateLake(Controller controller, HexGrid grid, Coordinate origin, RandomProvider random) {
        createWater(controller, grid, origin);
        createWater(controller, grid, new Coordinate(origin.getX(), origin.getY()+2));
        createWater(controller, grid, new Coordinate(origin.getX(), origin.getY()-2));
        createWater(controller, grid, new Coordinate(origin.getX(), origin.getY()-1));
        createWater(controller, grid, new Coordinate(origin.getX(), origin.getY()+1));
        if (origin.getY() % 2 == 0) {
            createWater(controller, grid, new Coordinate(origin.getX()+1, origin.getY()-1));
            createWater(controller, grid, new Coordinate(origin.getX()+1, origin.getY()+1));
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()+1, origin.getY()+2));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()-1, origin.getY()+2));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()+1, origin.getY()-2));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()-1, origin.getY()-2));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()-1, origin.getY()));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()+1, origin.getY()));
            }
        } else {
            createWater(controller, grid, new Coordinate(origin.getX()-1, origin.getY()-1));
            createWater(controller, grid, new Coordinate(origin.getX()-1, origin.getY()+1));
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()+1, origin.getY()+2));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()-1, origin.getY()+2));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()+1, origin.getY()-2));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()-1, origin.getY()-2));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()-1, origin.getY()));
            }
            if (random.flipCoin()) {
                createWater(controller, grid, new Coordinate(origin.getX()+1, origin.getY()));
            }
        }
    }
    
    private static void createWater(Controller controller, HexGrid grid, Coordinate position) {
        EntityGridNode node = (EntityGridNode) grid.getNode(position);
        if (node != null && !node.isObstacle()) {
            WaterEntity water = new WaterEntity(controller, "Water", position);
            water.init();
            node.removeEntity(node.getEntities().get(0));
            node.addEntity(water);
        }
    }
    
    private static void generateForest(Controller controller, HexGrid grid, Coordinate origin, RandomProvider random) {
        createTree(controller, grid, origin, random);
        createTree(controller, grid, new Coordinate(origin.getX(), origin.getY()+2), random);
        createTree(controller, grid, new Coordinate(origin.getX(), origin.getY()-2), random);
        createTree(controller, grid, new Coordinate(origin.getX(), origin.getY()-1), random);
        createTree(controller, grid, new Coordinate(origin.getX(), origin.getY()+1), random);
        if (origin.getY() % 2 == 0) {
            createTree(controller, grid, new Coordinate(origin.getX()+1, origin.getY()-1), random);
            createTree(controller, grid, new Coordinate(origin.getX()+1, origin.getY()+1), random);
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()+1, origin.getY()+2), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()-1, origin.getY()+2), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()+1, origin.getY()-2), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()-1, origin.getY()-2), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()-1, origin.getY()), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()+1, origin.getY()), random);
            }
        } else {
            createTree(controller, grid, new Coordinate(origin.getX()-1, origin.getY()-1), random);
            createTree(controller, grid, new Coordinate(origin.getX()-1, origin.getY()+1), random);
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()+1, origin.getY()+2), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()-1, origin.getY()+2), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()+1, origin.getY()-2), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()-1, origin.getY()-2), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()-1, origin.getY()), random);
            }
            if (random.flipCoin()) {
                createTree(controller, grid, new Coordinate(origin.getX()+1, origin.getY()), random);
            }
        }
    }
    
    private static void createTree(Controller controller, HexGrid grid, Coordinate position, RandomProvider random) {
        EntityGridNode node = (EntityGridNode) grid.getNode(position);
        if (node != null && !node.isObstacle()) {
            if (random.flipCoin()) {
                TreeEntity tree = new TreeEntity(controller, "Tree", position, TreeEntity.MAX_GROW);
                tree.init();
                node.addEntity(tree);
                tree.setDrawPriority(2);
            } else {
                TreeEntity tree = new FruitTreeEntity(controller, "FruitTree", position, TreeEntity.MAX_GROW);
                tree.init();
                node.addEntity(tree);
                tree.setDrawPriority(2);
            }
        }
    }
    
}
