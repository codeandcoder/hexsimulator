package net.codeandcoder.hexsimulator;

import java.util.List;

import net.codeandcoder.hexsimulator.UI.UIAdapter;
import net.codeandcoder.hexsimulator.entities.Entity;
import net.codeandcoder.hexsimulator.grid.EntityGridNode;
import net.codeandcoder.hexsimulator.grid.GridNode;
import net.codeandcoder.hexsimulator.grid.HexGrid;
import net.codeandcoder.hexsimulator.pathfinding.HexPathfinder;
import net.codeandcoder.hexsimulator.utils.RandomProvider;
import net.codeandcoder.hexsimulator.utils.ResourceLoader;

public class Controller {
    
    private HexGrid grid;
    private UIAdapter ui;
    private HexPathfinder pathfinder;
    private ResourceLoader resources;
    private RandomProvider randomProvider;
    
    public Controller(HexGrid grid, UIAdapter ui, HexPathfinder pathfinder, ResourceLoader resources, RandomProvider randomProvider) {
        this.grid = grid;
        this.ui = ui;
        this.pathfinder = pathfinder;
        this.resources = resources;
        this.randomProvider = randomProvider;
    }
    
    public void update(int iteration) {
        for (int y = 0; y < grid.getSizeY(); y++) {
            for (int x = -grid.getSizeY(); x < grid.getSizeX(); x++) {
                GridNode node = grid.getNode(x,y);
                
                if (node instanceof EntityGridNode) {
                    EntityGridNode entityNode = (EntityGridNode) node;
                    List<Entity> entities = entityNode.getEntities();
                    for (int i = 0; i < entities.size(); i++) {
                        entities.get(i).update(iteration);
                    }
                }
            }
        }
    }
    
    public void render() {
        ui.renderGrid(grid);
    }

    public HexGrid getGrid() {
        return grid;
    }

    public HexPathfinder getPathfinder() {
        return pathfinder;
    }
    
    public ResourceLoader getResources() {
        return resources;
    }
    
    public UIAdapter getUI() {
        return ui;
    }
    
    public RandomProvider getRandomProvider() {
        return randomProvider;
    }
    
}
