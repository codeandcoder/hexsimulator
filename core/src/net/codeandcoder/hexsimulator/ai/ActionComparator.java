package net.codeandcoder.hexsimulator.ai;

import java.util.Comparator;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.ai.actions.Action;

public class ActionComparator implements Comparator<Action> {
    
    private Controller controller;
    
    public ActionComparator(Controller controller) {
        this.controller = controller;
    }

    @Override
    public int compare(Action o1, Action o2) {
        int cost1 = o1.calculateCost(controller);
        int cost2 = o2.calculateCost(controller);
        
        return cost2 - cost1;
    }

}
