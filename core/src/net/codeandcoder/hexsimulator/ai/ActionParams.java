package net.codeandcoder.hexsimulator.ai;

import java.util.HashMap;
import java.util.Map;

import net.codeandcoder.hexsimulator.entities.Entity;

public class ActionParams {

    private Entity agent;
    private Map<String, Object> params = new HashMap<String, Object>();
    
    public ActionParams(Entity agent) {
        this.agent = agent;
    }
    
    public Object getParam(String key) {
        return params.get(key);
    }
    
    public ActionParams setParam(String key, Object value) {
        params.put(key, value);
        return this;
    }
    
    public ActionParams setParams(Map<String, Object> vars) {
        for (Map.Entry<String, Object> e : vars.entrySet()) {
            params.put(e.getKey(), e.getValue());
        }
        return this;
    }
    
    public Entity getAgent() {
        return agent;
    }
    
}
