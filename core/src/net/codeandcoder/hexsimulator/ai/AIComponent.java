package net.codeandcoder.hexsimulator.ai;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.ai.goals.Goal;
import net.codeandcoder.hexsimulator.entities.Entity;

public class AIComponent {

    private Controller controller;
    private Entity agent;
    private List<Goal> goals = new ArrayList<Goal>();
    private Goal activeGoal;
    
    public AIComponent(Controller controller, Entity agent) {
        this.controller = controller;
        this.agent = agent;
    }
    
    public void think(int iteration) {
        if (activeGoal == null || activeGoal.isFinished()) {
            if (!goals.isEmpty()) {
                activeGoal = goals.remove(0);
                activeGoal.buildActionPlan(controller);
            }
        } else {
            activeGoal.execute(controller, iteration);
        }
    }
    
    public void addGoal(Goal goal) {
        goals.add(goal);
        Collections.sort(goals);
    }
    
    public int getGoalCount() {
        return goals.size();
    }
    
    public Entity getAgent() {
        return agent;
    }
    
    public boolean hasGoal(String goalName) {
        for (Goal g : goals) {
            if (g.getName().equals(goalName)) {
                return true;
            }
        }
        
        return false;
    }
    
}
