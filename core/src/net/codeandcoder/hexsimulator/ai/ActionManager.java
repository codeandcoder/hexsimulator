package net.codeandcoder.hexsimulator.ai;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.ai.actions.Action;
import net.codeandcoder.hexsimulator.ai.actions.Eat;
import net.codeandcoder.hexsimulator.ai.actions.MoveToPoint;

public class ActionManager {
    
    public static Action findBestActions(Controller controller, ActionParams params, Map<String,Object> goals) {
        List<Action> actions = findPossibleActions(params);
        List<Action> candidates = new ArrayList<Action>();
        for (Action a : actions) {
            boolean isCandidate = true;
            for (Map.Entry<String, Object> entry : goals.entrySet()) {
                if (!a.getEffects().containsKey(entry.getKey())
                        || !a.getEffects().get(entry.getKey()).equals(entry.getValue())) {
                    
                    isCandidate = false;
                    break;
                }
            }
            if (isCandidate) {
                try {
                    candidates.add(a.getClass().getConstructor(String.class, ActionParams.class).newInstance("MoveToPoint", params));
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                } catch (NoSuchMethodException e) {
                    e.printStackTrace();
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        }
        
        if (!candidates.isEmpty()) {
            Collections.sort(candidates, new ActionComparator(controller));
            return candidates.get(0);
        }
        return null;
    }
    
    private static List<Action> findPossibleActions(ActionParams params) {
        List<Action> actions = new ArrayList<Action>();
        actions.add(new MoveToPoint("MoveToPoint", params));
        actions.add(new Eat("Eat", params));
        return actions;
    }

}
