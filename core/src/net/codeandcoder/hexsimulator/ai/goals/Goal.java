package net.codeandcoder.hexsimulator.ai.goals;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.ai.ActionManager;
import net.codeandcoder.hexsimulator.ai.ActionParams;
import net.codeandcoder.hexsimulator.ai.actions.Action;
import net.codeandcoder.hexsimulator.ai.actions.Action.ActionStatus;

public class Goal implements Comparable<Goal> {

    protected String name;
    protected Map<String, Object> conditions;
    protected int priority;
    protected List<Action> actions;
    protected Action activeAction;
    protected ActionParams params;
    protected boolean finished = false;
    
    public Goal(String name, int priority, Map<String, Object> conditions, ActionParams params) {
        this.name = name;
        this.priority = priority;
        this.conditions = conditions;
        this.params = params;
    }
    
    public void buildActionPlan(Controller controller) {
        actions = new ArrayList<Action>();
        findActions(controller, conditions);
        activeAction = actions.remove(0);
    }
    
    private void findActions(Controller controller, Map<String, Object> goals) {
        Action currentActions = ActionManager.findBestActions(controller, params, goals);
        //for (int i = 0; i < currentActions.size(); i++) {
        //    Action a = currentActions.get(i);
            actions.add(0, currentActions);
            if (!currentActions.getConditions().isEmpty()) {
                findActions(controller, currentActions.getConditions());
            }
        //}
    }
    
    public void execute(Controller controller, int iteration) {
        if (activeAction != null) {
            ActionStatus status = activeAction.execute(controller, iteration);
            if (status == ActionStatus.SUCCESSFUL) {
                if (actions.isEmpty()) {
                    activeAction = null;
                    finished = true;
                } else {
                    activeAction = actions.remove(0);
                }
            } else if (status == ActionStatus.FAILED) {
                activeAction = null;
                finished = true;
            }
        }
    }
    
    public String getName() {
        return name;
    }
    
    public int getPriority() {
        return priority;
    }
    
    public Map<String, Object> getConditions() {
        return conditions;
    }
    
    public boolean isFinished() {
        return finished;
    }
    
    @Override
    public int compareTo(Goal other) {
        return other.getPriority() - priority;
    }
    
    
}
