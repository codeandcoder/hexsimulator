package net.codeandcoder.hexsimulator.ai.goals;

import java.util.Map;

import net.codeandcoder.hexsimulator.ai.ActionParams;
import net.codeandcoder.hexsimulator.ai.actions.Action;
import net.codeandcoder.hexsimulator.entities.Entity;

import com.badlogic.gdx.math.Vector2;

public class GoalProvider {

    public static final String MOVE_TO_POINT = "Move";
    public static final String FEED = "Feed";
    
    public static Goal genGoal(String type, Entity agent, Map<String,Object> conditions, Map<String, Object> vars, int priority) {
        Goal goal = null;
        
        if (type.equals(MOVE_TO_POINT)) {
            ActionParams params = new ActionParams(agent);
            params.setParams(vars);
            goal = new Goal(MOVE_TO_POINT, priority, conditions, params);
        } else if (type.equals(FEED)) {
            ActionParams params = new ActionParams(agent);
            Entity feeder = (Entity) vars.get(Action.P_FEEDER);
            Vector2 position = new Vector2(feeder.getPosition().getX(), feeder.getPosition().getY());
            params.setParam(Action.P_FEEDER, feeder);
            params.setParam(Action.P_POINT, position);
            goal = new Goal(FEED, priority, conditions, params);
        }
        
        return goal;
    }
    
}
