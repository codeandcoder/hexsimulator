package net.codeandcoder.hexsimulator.ai.actions;

import java.util.Map;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.ai.ActionParams;

public abstract class Action {

    public static final String C_MOVED = "moved";
    public static final String C_FEED = "feed";
    
    public static final String P_POINT = "point";
    public static final String P_FEEDER = "feeder";
    
    public static enum ActionStatus { WORKING, FAILED, SUCCESSFUL }
    
    protected String name;
    protected ActionParams params;
    
    public Action(String name, ActionParams params) {
        this.name = name;
        this.params = params;
    }
    
    public abstract Map<String, Object> getConditions();
    public abstract Map<String, Object> getEffects();
    public abstract ActionStatus execute(Controller controller, int iteration);
    public abstract int calculateCost(Controller controller);
    
}
