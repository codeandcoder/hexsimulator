package net.codeandcoder.hexsimulator.ai.actions;

import java.util.HashMap;
import java.util.Map;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.ai.ActionParams;
import net.codeandcoder.hexsimulator.ai.goals.GoalProvider;
import net.codeandcoder.hexsimulator.entities.Entity;
import net.codeandcoder.hexsimulator.entities.interfaces.Eater;
import net.codeandcoder.hexsimulator.entities.interfaces.Feeder;
import net.codeandcoder.hexsimulator.entities.interfaces.Thinker;

public class Eat extends Action {

    public Eat(String name, ActionParams params) {
        super(name, params);
    }
    
    @Override
    public Map<String, Object> getConditions() {
        Map<String, Object> conds = new HashMap<String, Object>();
        conds.put(Action.C_MOVED, true);
        return conds;
    }

    @Override
    public Map<String, Object> getEffects() {
        Map<String, Object> effs = new HashMap<String, Object>();
        effs.put(Action.C_FEED, true);
        return effs;
    }

    @Override
    public ActionStatus execute(Controller controller, int iteration) {
        Eater eater = (Eater) params.getAgent();
        Feeder feeder = (Feeder) params.getParam("feeder");
        feeder.consume(eater, 5);
        eater.eat(feeder, 5);
        if (eater.isSatisfied()) {
            return ActionStatus.SUCCESSFUL;
        } else if (feeder.isExhausted()) {
            Feeder f = controller.getPathfinder().findNearestFeeder(params.getAgent().getPosition());
            if (f == null) {
                return ActionStatus.FAILED;
            } else {
                Map<String, Object> conds = new HashMap<String, Object>();
                conds.put(Action.C_FEED, true);
                Map<String, Object> vars = new HashMap<String, Object>();
                vars.put(Action.P_FEEDER, f);
                ((Thinker)params.getAgent()).getBrain().addGoal(GoalProvider.genGoal(GoalProvider.FEED, params.getAgent(), conds, vars, 10));
                return ActionStatus.SUCCESSFUL;
            }
            
        }
        
        return ActionStatus.WORKING;
    }

    @Override
    public int calculateCost(Controller controller) {
        Entity feeder = (Entity) params.getParam("feeder");
        Entity agent = params.getAgent();
        int xDifference = (int) Math.abs(feeder.getPosition().getX() - agent.getPosition().getX());
        int yDifference = (int) Math.abs(feeder.getPosition().getY() - agent.getPosition().getY());
        return (xDifference + yDifference);
    }
    
}
