package net.codeandcoder.hexsimulator.ai.actions;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.ai.ActionParams;
import net.codeandcoder.hexsimulator.entities.interfaces.Movable;
import net.codeandcoder.hexsimulator.grid.Coordinate;
import net.codeandcoder.hexsimulator.grid.EntityGridNode;
import net.codeandcoder.hexsimulator.grid.GridNode;

import com.badlogic.gdx.math.Vector2;

public class MoveToPoint extends Action {

    List<Coordinate> currentPath;
    
    public MoveToPoint(String name, ActionParams params) {
        super(name, params);
    }

    @Override
    public Map<String, Object> getConditions() {
        Map<String, Object> conds = new HashMap<String, Object>();
        return conds;
    }

    @Override
    public Map<String, Object> getEffects() {
        Map<String, Object> effs = new HashMap<String, Object>();
        effs.put(C_MOVED, true);
        return effs;
    }

    @Override
    public ActionStatus execute(Controller controller, int iteration) {
        if (currentPath == null) {
            currentPath = initPath(controller);
            if (distanceBetweenPoints(controller) <= 1) {
                return ActionStatus.SUCCESSFUL;
            }
            if (currentPath.isEmpty()) {
                return ActionStatus.FAILED;
            }
        }
        if (currentPath.isEmpty()) {
            return ActionStatus.SUCCESSFUL;
        } else {
            Coordinate target = currentPath.remove(0);
            EntityGridNode node = (EntityGridNode) controller.getGrid().getNode(target);
            if (node.isObstacle()) {
                currentPath = initPath(controller);
            } else {
                if (params.getAgent() instanceof Movable) {
                    ((Movable) params.getAgent()).move(target);
                }
            }
        }
        
        return ActionStatus.WORKING;
    }
    
    protected List<Coordinate> initPath(Controller controller) {
        Vector2 targetPosition = (Vector2) params.getParam(P_POINT);
        Coordinate pointA = params.getAgent().getPosition();
        Coordinate pointB = new Coordinate((int)targetPosition.x, (int)targetPosition.y);
        GridNode origin = controller.getGrid().getNode(pointA);
        List<Coordinate> currentPath = controller.getPathfinder().findPath(origin, pointB);
        return currentPath;
    }
    
    protected int distanceBetweenPoints(Controller controller) {
        Vector2 targetPosition = (Vector2) params.getParam(P_POINT);
        Coordinate pointA = params.getAgent().getPosition();
        Coordinate pointB = new Coordinate((int)targetPosition.x, (int)targetPosition.y);
        double distance = pointA.hexDistanceTo(pointB);
        distance -= controller.getGrid().getNode(pointB).isObstacle() ? 1 : 0;
        return (int) Math.round(distance);
    }

    @Override
    public int calculateCost(Controller controller) {
        Coordinate point = (Coordinate) params.getParam(P_POINT);
        if (point == null) {
            return 100;
        }
        return (int) params.getAgent().getPosition().hexDistanceTo(point);
    }

}
