package net.codeandcoder.hexsimulator.entities;

import java.util.HashMap;
import java.util.Map;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.ai.AIComponent;
import net.codeandcoder.hexsimulator.ai.actions.Action;
import net.codeandcoder.hexsimulator.ai.goals.GoalProvider;
import net.codeandcoder.hexsimulator.entities.interfaces.Eater;
import net.codeandcoder.hexsimulator.entities.interfaces.Feeder;
import net.codeandcoder.hexsimulator.entities.interfaces.Movable;
import net.codeandcoder.hexsimulator.entities.interfaces.Thinker;
import net.codeandcoder.hexsimulator.grid.Coordinate;
import net.codeandcoder.hexsimulator.grid.EntityGridNode;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class Person extends Entity implements Movable, Thinker, Eater {
    
    private AIComponent brain;
    
    protected int hunger;
    
    public enum DeathCause { Hunger };

    public Person(Controller controller, String name, Coordinate position) {
        super(controller, name, position);
        brain = new AIComponent(controller, this);
    }

    @Override
    public void init() {
        aspect = new Sprite(controller.getResources().getTexture("person"));
    }
    
   @Override
   public void act(int iteration) {
       brain.think(iteration);
       
       // Each 5 iterations, add hunger
       if (lifeTime % 5 == 0) {
           hunger++;
       }
       
       // Probabilities of going to eat
       if (lifeTime % 5 == 0 && !brain.hasGoal(GoalProvider.FEED)) {
           int random = controller.getRandomProvider().randomNum(0, 100);
           if (random <= hunger) {
               Feeder f = controller.getPathfinder().findNearestFeeder(position);
               if (f != null) {
                   Map<String, Object> conds = new HashMap<String, Object>();
                   conds.put(Action.C_FEED, true);
                   Map<String, Object> vars = new HashMap<String, Object>();
                   vars.put(Action.P_FEEDER, f);
                   brain.addGoal(GoalProvider.genGoal(GoalProvider.FEED, this, conds, vars, 10));
               }
           }
       }
       
       if (hunger == 100) {
           die(DeathCause.Hunger);
       }
   }
   
   public void die(DeathCause cause) {
       destroy();
   }

    @Override
    public boolean isObstacle() {
        return false;
    }

    @Override
    public void move(Coordinate target) {
        EntityGridNode originNode = (EntityGridNode) controller.getGrid().getNode(position);
        EntityGridNode targetNode = (EntityGridNode) controller.getGrid().getNode(target);
        originNode.removeEntity(this);
        targetNode.addEntity(this);
        position = target;
    }
    
    @Override
    public AIComponent getBrain() {
        return brain;
    }

    @Override
    public void eat(Feeder feeder, int amount) {
        hunger -= amount;
        hunger = hunger < 0 ? 0 : hunger;
    }

    @Override
    public boolean isSatisfied() {
        return hunger == 0;
    }

    @Override
    public boolean canMove() {
        return true;
    }

}
