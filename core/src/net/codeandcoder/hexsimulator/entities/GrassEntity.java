package net.codeandcoder.hexsimulator.entities;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.grid.Coordinate;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class GrassEntity extends Entity {

    public GrassEntity(Controller controller, String name, Coordinate position) {
        super(controller, name, position);
    }

    @Override
    public void init() {
        aspect = new Sprite(controller.getResources().getTexture("grass"));
    }

    @Override
    public boolean isObstacle() {
        return false;
    }

    @Override
    public void act(int iteration) {
    }

}
