package net.codeandcoder.hexsimulator.entities;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.grid.Coordinate;
import net.codeandcoder.hexsimulator.grid.EntityGridNode;
import net.codeandcoder.hexsimulator.utils.RandomProvider;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class TreeEntity extends Entity {

    public static final int MIN_REPR_RATE = 750;
    public static final int MAX_REPR_RATE = 1250;
    public static final int MIN_GROW_RATE = 180;
    public static final int MAX_GROW_RATE = 220;
    public static final int MIN_RESISTANCE = 1;
    public static final int MAX_RESISTANCE = 3;
    public static final int MAX_GROW = 2;
    
    protected int reproductionRate;
    protected int growingRate;
    protected int resistance;
    protected int growingState = 0;
    protected int offsprings = 1;
    
    public TreeEntity(Controller controller, String name, Coordinate position) {
        super(controller, name, position);
        reproductionRate = controller.getRandomProvider().randomNum(MIN_REPR_RATE, MAX_REPR_RATE);
        growingRate = controller.getRandomProvider().randomNum(MIN_GROW_RATE, MAX_GROW_RATE);
        resistance = controller.getRandomProvider().randomNum(MIN_RESISTANCE, MAX_RESISTANCE);
    }
    
    public TreeEntity(Controller controller, String name, Coordinate position, int growingState) {
        super(controller, name, position);
        reproductionRate = controller.getRandomProvider().randomNum(MIN_REPR_RATE, MAX_REPR_RATE);
        growingRate = controller.getRandomProvider().randomNum(MIN_GROW_RATE, MAX_GROW_RATE);
        resistance = controller.getRandomProvider().randomNum(MIN_RESISTANCE, MAX_RESISTANCE);
        this.growingState = growingState;
    }

    @Override
    public void init() {
        aspect = new Sprite(controller.getResources().getTexture("tree"));
        aspect.scale(-0.5f + 0.25f * growingState);
    }

    @Override
    public boolean isObstacle() {
        return true;
    }

    @Override
    public void act(int iteration) {
        if (lifeTime % growingRate == 0 && growingState < MAX_GROW) {
            grow();
        }
        
        if (lifeTime % reproductionRate == 0 && growingState == MAX_GROW) {
            reproduce();
        }
    }
    
    public void grow() {
        resistance++;
        growingState++;
        aspect.scale(0.25f);
    }
    
    public void reproduce() {
        RandomProvider random = controller.getRandomProvider();
        for (int i = 0; i < offsprings; i++) {
            int num = controller.getRandomProvider().randomNum(0, 6);
            Coordinate coord = position.surroundingfromNumber(num);
            int tryCounter = 0;
            while (tryCounter < 8) {
                tryCounter++;
                EntityGridNode entityNode = (EntityGridNode) controller.getGrid().getNode(coord);
                if (!entityNode.isObstacle()) {
                    if (random.flipCoin()) {
                        TreeEntity tree = new TreeEntity(controller, "Tree", coord);
                        tree.init();
                        entityNode.addEntity(tree);
                        tree.setDrawPriority(2);
                    } else {
                        TreeEntity tree = new FruitTreeEntity(controller, "FruitTree", coord);
                        tree.init();
                        entityNode.addEntity(tree);
                        tree.setDrawPriority(2);
                    }
                    break;
                } else {
                    num = (num + 1) % 8;
                    coord = position.surroundingfromNumber(num);
                }
            }
        }
    }

}
