package net.codeandcoder.hexsimulator.entities;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.entities.interfaces.Eater;
import net.codeandcoder.hexsimulator.entities.interfaces.Feeder;
import net.codeandcoder.hexsimulator.grid.Coordinate;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class FruitTreeEntity extends TreeEntity implements Feeder {

    private static final int MAX_FOOD = 10;
    private static final int MIN_FOOD_RATE = 120;
    private static final int MAX_FOOD_RATE = 180;
    
    private int foodRate;
    private int food = 0;
    
    public FruitTreeEntity(Controller controller, String name, Coordinate position) {
        super(controller, name, position);
        foodRate = controller.getRandomProvider().randomNum(MIN_FOOD_RATE, MAX_FOOD_RATE);
    }
    
    public FruitTreeEntity(Controller controller, String name, Coordinate position, int growingState) {
        super(controller, name, position, growingState);
        foodRate = controller.getRandomProvider().randomNum(MIN_FOOD_RATE, MAX_FOOD_RATE);
    }

    @Override
    public void init() {
        super.init();
    }

    @Override
    public void act(int iteration) {
        super.act(iteration);
        
     // Each 10 iterations, grow food
        if (growingState == 2 && lifeTime % foodRate == 0 && food < MAX_FOOD) {
            if (food == 0) {
                aspect = new Sprite(controller.getResources().getTexture("fruit-tree"));
            }
            food++;
        }
    }

    @Override
    public void consume(Eater eater, int amount) {
        food -= amount;
        food = food < 0 ? 0 : food;
        if (food == 0) {
            aspect = new Sprite(controller.getResources().getTexture("tree"));
        }
    }

    @Override
    public boolean isExhausted() {
        return food == 0;
    }

}
