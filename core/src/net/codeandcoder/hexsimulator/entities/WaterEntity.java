package net.codeandcoder.hexsimulator.entities;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.grid.Coordinate;

import com.badlogic.gdx.graphics.g2d.Sprite;

public class WaterEntity extends Entity {

    public WaterEntity(Controller controller, String name, Coordinate position) {
        super(controller, name, position);
    }

    @Override
    public void init() {
        aspect = new Sprite(controller.getResources().getTexture("water"));
    }

    @Override
    public boolean isObstacle() {
        return true;
    }

    @Override
    public void act(int iteration) {
    }

}
