package net.codeandcoder.hexsimulator.entities.interfaces;

import net.codeandcoder.hexsimulator.ai.AIComponent;

public interface Thinker {

    public abstract AIComponent getBrain();
    
}
