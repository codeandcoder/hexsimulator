package net.codeandcoder.hexsimulator.entities.interfaces;


public interface Feeder {

    public abstract void consume(Eater eater, int amount);
    public abstract boolean isExhausted();
    
}
