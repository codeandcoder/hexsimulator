package net.codeandcoder.hexsimulator.entities.interfaces;


public interface Eater {

    public abstract void eat(Feeder feeder, int amount);
    public abstract boolean isSatisfied();
    
}
