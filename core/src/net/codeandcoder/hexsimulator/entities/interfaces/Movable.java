package net.codeandcoder.hexsimulator.entities.interfaces;

import net.codeandcoder.hexsimulator.grid.Coordinate;

public interface Movable {

    public abstract void move(Coordinate target);
    public abstract boolean canMove();
    
}
