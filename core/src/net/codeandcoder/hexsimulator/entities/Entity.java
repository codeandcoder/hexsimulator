package net.codeandcoder.hexsimulator.entities;

import net.codeandcoder.hexsimulator.Controller;
import net.codeandcoder.hexsimulator.grid.Coordinate;
import net.codeandcoder.hexsimulator.grid.EntityGridNode;

import com.badlogic.gdx.graphics.g2d.Sprite;

public abstract class Entity {
    
    private static long currentID = 0;
    protected long id;
    protected String name;
    protected Coordinate position;
    protected int drawPriority;
    protected Sprite aspect;
    protected Controller controller;
    private int lastIteration = -1;
    protected int lifeTime = 0;
    
    public Entity(Controller controller, String name, Coordinate position) {
        this.controller = controller;
        this.id = Entity.nextID();
        this.name = name;
        this.position = new Coordinate(position.getX(), position.getY());
    }

    // Called when created
    public abstract void init();
    public abstract void act(int iteration);
    public abstract boolean isObstacle();
    
    // Called each game iteration
    public final void update(int iteration) {
        if (iteration != lastIteration) {
            lifeTime++;
            act(iteration);
            lastIteration = iteration;
        }
    }
    
    public Coordinate getPosition() {
        return position;
    }
    
    public Sprite getAspect() {
        return aspect;
    }
    
    private static long nextID() {
        return currentID++;
    }

    public int getDrawPriority() {
        return drawPriority;
    }

    public void setDrawPriority(int drawPriority) {
        EntityGridNode node = (EntityGridNode) controller.getGrid().getNode(position);
        node.drawPriorityChanged();
        this.drawPriority = drawPriority;
    }
    
    // Must be called before removing all references
    public void destroy() {
        EntityGridNode entityNode = (EntityGridNode) controller.getGrid().getNode(position);
        entityNode.removeEntity(this);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int) (id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Entity other = (Entity) obj;
        if (id != other.id)
            return false;
        return true;
    }
    
}
