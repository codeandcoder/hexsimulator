package net.codeandcoder.hexsimulator;

import net.codeandcoder.hexsimulator.UI.GUIAdapter;
import net.codeandcoder.hexsimulator.UI.UIAdapter;

public class CommandInterpreter {

    public static String lastCommand = "command";
    
    public static void execute(HexSimulator game, String command) {
        if (command.contains("speed")) {
            String value = command.split(" ")[1].trim();
            float vel = Float.valueOf(value);
            HexSimulator.ITERATION_LENGTH = vel;
        } else if (command.contains("size")) {
            String value = command.split(" ")[1].trim();
            float size = Float.valueOf(value);
            UIAdapter ui = game.getController().getUI();
            if (ui instanceof GUIAdapter) {
                ((GUIAdapter)ui).setSquareSize(size);
            }
        } else if (command.contains("restart")) {
            String[] params = command.split(" ");
            String seed = params.length > 1 ? params[1] : "1234567";
            int sizeX = params.length > 2 ? Integer.parseInt(params[2]) : 30;
            int sizeY = params.length > 3 ? Integer.parseInt(params[3]) : 60;
            game.restart(seed, sizeX, sizeY);
        }
        
        lastCommand = command;
    }
    
}
