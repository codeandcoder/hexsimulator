package net.codeandcoder.hexsimulator.UI;

import net.codeandcoder.hexsimulator.CommandInterpreter;
import net.codeandcoder.hexsimulator.HexSimulator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.TextInputListener;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;

public class GameScreen extends GUIScreen implements EventListener {

    private Stage stage;
    private TextButton button;
    
    public GameScreen(HexSimulator game) {
        super(game);
    }
    
    @Override
    protected void init() {
        stage = new Stage();
        Gdx.input.setInputProcessor(stage);
        
        Table table = new Table();
        table.setFillParent(true);
        stage.addActor(table);
        
        button = new TextButton("Command", skin.get("commandButton", TextButtonStyle.class));
        button.setPosition(Gdx.graphics.getWidth() - button.getWidth() - 5, 0);
        button.addListener(this);
        table.addActor(button);
        
        table.pack();
    }

    @Override
    public void render(SpriteBatch batch) {
        stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
        stage.draw();
    }

    @Override
    public void destroy() {
    }

    @Override
    public boolean handle(Event event) {
        if (event instanceof ChangeEvent) {
            if (event.getListenerActor().equals(button)) {
                CommandInputListener listener = new CommandInputListener();
                Gdx.input.getTextInput(listener, "Execute", CommandInterpreter.lastCommand);
            }
        }
        
        return false;
    }
    
    private class CommandInputListener implements TextInputListener {
        @Override
        public void input (String text) {
            CommandInterpreter.execute((HexSimulator) game, text.toLowerCase().trim());
        }

        @Override
        public void canceled () {
        }
     }

}
