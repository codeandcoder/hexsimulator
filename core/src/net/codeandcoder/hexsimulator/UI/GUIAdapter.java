package net.codeandcoder.hexsimulator.UI;

import net.codeandcoder.hexsimulator.entities.Entity;
import net.codeandcoder.hexsimulator.grid.EntityGridNode;
import net.codeandcoder.hexsimulator.grid.GridNode;
import net.codeandcoder.hexsimulator.grid.HexGrid;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

public class GUIAdapter implements UIAdapter {
    
    private static final Color CLEAR_COLOR = new Color(0,0,0,1);
    private OrthographicCamera camera;
    private SpriteBatch batch;
    private float squareSize;
    //private BitmapFont font = new BitmapFont();

    public GUIAdapter(float squareSize, OrthographicCamera camera, SpriteBatch batch) {
        this.batch = batch;
        this.squareSize = squareSize;
        this.camera = camera;
    }
    
    @Override
    public void renderGrid(HexGrid grid) {
        camera.update();
        batch.setProjectionMatrix(camera.combined);
        Gdx.gl.glClearColor(CLEAR_COLOR.r, CLEAR_COLOR.g, CLEAR_COLOR.b, CLEAR_COLOR.a);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        batch.begin();
        
        for (int y = 0; y < grid.getSizeY(); y++) {
            for (int x = 0; x < grid.getSizeX(); x++) {
                GridNode node = grid.getNode(x,y);
                
                if (node instanceof EntityGridNode) {
                    EntityGridNode entityNode = (EntityGridNode) node;
                    for (Entity e : entityNode.getEntities()) {
                        renderEntity(e);
                    }
                }
            }
        }
        
        batch.end();
    }

    @Override
    public void renderEntity(Entity entity) {
        Sprite aspect = entity.getAspect();
        if (aspect != null) {
            float size = squareSize;
            aspect.setSize(size, size);
            aspect.setOrigin(0, 0);
            int x = entity.getPosition().getX();
            int y = entity.getPosition().getY();
            if (y % 2 == 0) {
                aspect.setPosition((x * size + x * size/2 + size * 3 / 4) + (1 - aspect.getScaleX() * size/2), (y * size / 2)  + (1 - aspect.getScaleY() * size/2));
            } else {
                aspect.setPosition((x * size + x * size/2) + (1 - aspect.getScaleX() * size/2), (y * size / 2)  + (1 - aspect.getScaleY() * size/2)); 
            }
            //font.draw(batch, "" + x, aspect.getX() + size / 3, aspect.getY());
            aspect.draw(batch);
        }
    }
    
    public void setSquareSize(float size) {
        squareSize = size;
    }
    
    public EntityGridNode getNodeInPixels(HexGrid grid, int targetX, int targetY) {
        Vector3 touchpoint = new Vector3();
        for (int y = 0; y < grid.getSizeY(); y++) {
            for (int x = 0; x < grid.getSizeX(); x++) {
                GridNode node = grid.getNode(x,y);
                
                if (node instanceof EntityGridNode) {
                    EntityGridNode entityNode = (EntityGridNode) node;
                    if (entityNode.getEntities().size() > 0) {
                        Entity ent = entityNode.getEntities().get(0);
                        camera.unproject(touchpoint.set(Gdx.input.getX(),Gdx.input.getY(),0));
                        if (ent.getAspect().getBoundingRectangle().contains(touchpoint.x, touchpoint.y)) {
                            return entityNode;
                        }
                    }
                }
            }
        }
        
        return null;
    }

}
