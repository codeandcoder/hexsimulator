package net.codeandcoder.hexsimulator.UI;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import net.codeandcoder.hexsimulator.HexSimulator;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public abstract class GUIScreen {

    private static Map<Class<? extends GUIScreen>,GUIScreen> screens = new HashMap<Class<? extends GUIScreen>,GUIScreen>();
    
    public static <T extends GUIScreen> GUIScreen buildScreen(HexSimulator game, Class<? extends GUIScreen> c) {
        GUIScreen screen = screens.get(c);
        if (screen == null) {
            try {
                screen = c.getConstructor(HexSimulator.class).newInstance(game);
                screens.put(c, screen);
            } catch (NoSuchMethodException ex) {
                ex.printStackTrace();
            } catch (InvocationTargetException ex) {
                ex.printStackTrace();
            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            } catch (InstantiationException ex) {
                ex.printStackTrace();
            }
        }
        
        return screen;
    }
    
    protected HexSimulator game;
    protected Skin skin;
    
    protected GUIScreen(HexSimulator game) {
        this.game = game;
        this.skin = SkinBuilder.buildSkin();
        this.init();
    }
    
    protected abstract void init();
    public abstract void render(SpriteBatch batch);
    public abstract void destroy();
    
}
