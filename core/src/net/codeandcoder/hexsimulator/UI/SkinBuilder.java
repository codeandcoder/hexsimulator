package net.codeandcoder.hexsimulator.UI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;

public class SkinBuilder {

    private static Skin skin;
    
    public static Skin buildSkin() {
        if (skin == null) {
            skin = initSkin();
        }
        
        return skin;
    }
    
    private static Skin initSkin() {
        Skin skin = new Skin();
        
        BitmapFont font = new BitmapFont(Gdx.files.internal("font.fnt"));
        skin.add("default", font);
        skin.getFont("default").scale(-0.2f);
        skin.getFont("default").getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        
        BitmapFont bigFont = new BitmapFont(Gdx.files.internal("font.fnt"));
        skin.add("big", bigFont);
        skin.getFont("big").scale(0.18f);
        skin.getFont("big").getRegion().getTexture().setFilter(TextureFilter.Linear, TextureFilter.Linear);
        
        TextButtonStyle commandButtonStyle = new TextButtonStyle();
        commandButtonStyle.font = skin.getFont("default");
        commandButtonStyle.fontColor = Color.WHITE;
        skin.add("commandButton", commandButtonStyle);
        
        return skin;
    }
    
}
