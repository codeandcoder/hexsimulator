package net.codeandcoder.hexsimulator.UI;

import net.codeandcoder.hexsimulator.entities.Entity;
import net.codeandcoder.hexsimulator.grid.HexGrid;

public interface UIAdapter {

    public abstract void renderGrid(HexGrid grid);
    public abstract void renderEntity(Entity entity);
    
}
