package net.codeandcoder.hexsimulator;

import net.codeandcoder.hexsimulator.utils.ResourceLoader;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public abstract class Game extends ApplicationAdapter {

    protected boolean initialized = false;
    private boolean showingSplashScreen = false;
    private Texture splashScreen;
    
    protected SpriteBatch batch;
    protected OrthographicCamera camera;
    
    protected ResourceLoader resources;
    
    /*
     * Called at the very beginning.
     * 
     * @see com.badlogic.gdx.ApplicationAdapter#create()
     */
    @Override
    public final void create () {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        batch = new SpriteBatch();
        resources = getResourceLoader();
        splashScreen = resources.getNoLoadedTexture(getSplashScreenPath());
        
        // Camera initialization
        float w = Gdx.graphics.getWidth();
        float h = Gdx.graphics.getHeight();
        camera = new OrthographicCamera(w, h);
        camera.position.set(camera.viewportWidth / 2f, camera.viewportHeight / 2f, 0);
        camera.update();
    }

    /*
     * Called every frame. 
     * It manages the splash screen showing and the calling to Init() method.
     * 
     * @see com.badlogic.gdx.ApplicationAdapter#render()
     */
    @Override
    public final void render () {
        if (showingSplashScreen) {
            showingSplashScreen = false;
            resources.initResources();
            init();
            initialized = true;
        }
        
        if (initialized)
            update(Gdx.graphics.getDeltaTime());
        
        if (!initialized) {
            camera.update();
            batch.setProjectionMatrix(camera.combined);
            batch.begin();
            drawSplashScreen(batch);
            showingSplashScreen = true;
            batch.end();
        }
        
        if (initialized)
            renderGUI();
    }
    
    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
        camera.update();
    }
    
    @Override
    public final void dispose() {
        super.dispose();
        
        resources.dispose();
        destroy();
    }
    
    private void drawSplashScreen(SpriteBatch batch) {
        if (splashScreen != null) {
            batch.draw(splashScreen,0,0,Gdx.graphics.getWidth(),Gdx.graphics.getHeight());
        }
    }
    
    protected abstract void init();
    protected abstract void update(float deltaTime);
    protected abstract void renderGUI();
    protected abstract void destroy();
    protected abstract String getSplashScreenPath();
    protected abstract ResourceLoader getResourceLoader();
    
    // Getters && Setters
    
    public boolean isInitialized() {
        return initialized;
    }

    public ResourceLoader getResources() {
        return resources;
    }
    
}
